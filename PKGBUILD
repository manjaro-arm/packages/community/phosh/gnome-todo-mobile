# Maintainer: Jan Alexander Steffens (heftig) <heftig@archlinux.org>

pkgname=gnome-todo-mobile
_pkgname=gnome-todo
pkgver=41.0
pkgrel=2
pkgdesc="Task manager for GNOME"
url="https://wiki.gnome.org/Apps/Todo"
arch=(x86_64 aarch64)
license=(GPL)
depends=(evolution-data-server libpeas python gtk4 libportal-gtk4 libadwaita)
makedepends=(gobject-introspection appstream-glib git meson yelp-tools)
groups=(gnome-extra)
provides=(gnome-todo)
conflicts=(gnome-todo)
_commit=fe5f91502e57f0e945e8b72229db678212dcd9db  # tags/41.0^0
source=("git+https://gitlab.gnome.org/GNOME/gnome-todo.git#commit=$_commit"
flatpak-Adjust-to-libportal-changes.patch
build-compile-gsettings-schemas-for-use-in-build-tests.patch
theme-selector-Port-to-AdwStyleManager.patch
theme-selector-Improve-selector-style.patch
application-Subclass-AdwApplication.patch
css-provider-Adjust-to-AdwStyleManager.patch
window-Fix-indentation.patch
window-Remove-headerbar-box.patch
window-Move-headerbar-to-task-lists-workspace.patch
window-Move-leaflet-to-task-lists-workspace.patch
task-lists-workspace-Reintroduce-split-headerbars.patch
task-lists-workspace-Add-separator.patch
task-lists-workspace-Remove-sidebar-style-class.patch
task-list-workspace-Use-adw_leaflet_navigate.patch
task-lists-workspace-Rename-property.patch
fix-ui-scaling.patch
meson.build-make-sure-plugins-depend-on-generated-headers.patch)
sha256sums=('SKIP'
            'c7d627a9bd6ba2a486df1815eb51c3a6f421d0d5d96f5bc66017b90d163df2ca'
            '457212e8b24198eeaef3bd43a221dd3db1d5c309553fa31b3cfa5b7cbeffc8f7'
            'dde4d32d8e3ce3bd4375815d77413b3846c4939b97f88d21ceb22d413819eebf'
            '08c05e73671cd074ab6993591fa5c66a5b96e4a9776e0f931c3cd761b799acd8'
            '8b5de508d62a20a8128ff6118a52677553a3051f901b11a58bc3759f373894df'
            'c04b34736f6e8805b45c0d80e457c69eb2a69a9f0063c1261bf9e858b6d73e94'
            'dd38dba7843aa8f6631e907a3fc44feeed64062c98f544bb365c516f27ae8425'
            '13b8e357164bf194edd007b35c32c30ea7f04dbf0956f89c8a98211f06a2d9f3'
            '1435bd399139876e2174383e3a251a4cfb77e4b4a6d0aab2e374fd3b7b41fe38'
            '50b672090f911bf1bbb90758a78bd624a85e1a52ec01dfa51f91f37e03b6cbb8'
            '116adcdcfd1dc9825a992df0ebdec9288b0a7f2257f7376e9db30054564a95f9'
            '9f878bca18e391560a2a7f915b451199bf6bea5c16b10946294739fa5cf40570'
            '85ffe406678f6820de69127bff3fa1c304256f0cfaa51137b51b184368e7c3f6'
            '6d3ae69352ce7f9f26790726ace10a48f14edb5f686fa3b6013686f421c82ff7'
            '60ba154bec8df0c6aa65770053286f3089bdb94aad3105ea8b1ca43ec7f15ce3'
            'e4b74ce22adeb3eb85c7f1b2e8fdb22a44e89534f64be51f10f5734aa6a188b2'
            'd419074fc4ac060aba22232c99e54fdc361c5fb260a8944f51fbd9edac1eee6f')

pkgver() {
  cd $_pkgname
  git describe --tags | sed 's/^GNOME_TODO_//;s/_/./g;s/-/+/g'
}

prepare() {
  cd $_pkgname
  
  local src
  for src in "${source[@]}"; do
      src="${src%%::*}"
      src="${src##*/}"
      [[ $src = *.patch ]] || continue
      echo "Applying patch $src..."
      patch -Np1 < "../$src"
  done 
}

build() {
  arch-meson $_pkgname build
  meson compile -C build
}

package() {
  DESTDIR="$pkgdir" meson install -C build
}

# vim:set ts=2 sw=2 et:
